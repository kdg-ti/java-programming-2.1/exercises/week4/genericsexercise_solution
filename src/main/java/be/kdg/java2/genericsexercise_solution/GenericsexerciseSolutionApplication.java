package be.kdg.java2.genericsexercise_solution;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GenericsexerciseSolutionApplication {

    public static void main(String[] args) {
        SpringApplication.run(GenericsexerciseSolutionApplication.class, args);
    }

}
