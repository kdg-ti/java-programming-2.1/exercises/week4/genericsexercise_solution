package be.kdg.java2.genericsexercise_solution;

public class EmptyStackException extends RuntimeException {
    public EmptyStackException() {
        this("Stack is empty");
    }

    public EmptyStackException(String message) {
        super(message);
    }
}
