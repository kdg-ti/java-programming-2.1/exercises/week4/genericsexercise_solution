package be.kdg.java2.genericsexercise_solution;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class TestcodeRunner implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        System.out.println("Hello world");

        // Remove comments to test part 1
        LimitedStack<String> stringStack = new LimitedStack<>(2);
        try {
            stringStack.push("Boudewijn");
            stringStack.push("Albert");
            stringStack.push("Filip");
        }
        catch(FullStackException e) {
            System.out.println(e.getMessage());
        }
        System.out.printf("Stack as string: %s\n", stringStack);
        System.out.printf("Top element: %s\n", stringStack.top());
        System.out.printf("capacity = %d size = %d\n", stringStack.capacity(), stringStack.size());
        String str = stringStack.pop();
        System.out.printf("Remove top: %s\n", str);
        System.out.printf("After pop: capacity = %d size = %d\n", stringStack.capacity(), stringStack.size());
        System.out.printf("Stack as string: %s\n", stringStack);


        // Remove comments to test part 2
        LimitedNumericStack<Integer> intStack = new LimitedNumericStack<>();
        intStack.push(1);
        intStack.push(2);
        intStack.push(3);
        System.out.printf("\nStack as string: %s\n", intStack);
        System.out.printf("Top element: %d\n", intStack.top());
        System.out.printf("capacity = %d size = %d\n", intStack.capacity(), intStack.size());
        int i = intStack.pop();
        System.out.printf("Remove top: %d\n", i);
        System.out.printf("After pop: capacity = %d size = %d\n", intStack.capacity(), intStack.size());
        System.out.printf("Stack as string: %s\n", intStack);

        MyNumericStack<Integer> myIntStack = new MyNumericStack<>();
        myIntStack.push(1);
        myIntStack.push(2);
        myIntStack.push(3);
        System.out.printf("\nStack as string: %s\n", myIntStack);
        System.out.printf("Top element: %d\n", myIntStack.top());
        System.out.printf("capacity = %d size = %d\n", myIntStack.capacity(), myIntStack.size());
        i = myIntStack.pop();
        System.out.printf("Remove top: %d\n", i);
        System.out.printf("After pop: capacity = %d size = %d\n", myIntStack.capacity(), myIntStack.size());
        System.out.printf("Stack as string: %s\n", myIntStack);
    }
}
/*
EXPECTED RESULTY:

Stack is full; can not push Filip
Stack as string: [Boudewijn, Albert]
Top element: Albert
capacity = 2 size = 2
Remove top: Albert
After pop: capacity = 2 size = 1
Stack as string: [Boudewijn, ?]

Stack as string: [1, 2, 3, ?, ?, ?, ?, ?, ?, ?]
Top element: 3
capacity = 10 size = 3
Remove top: 3
After pop: capacity = 10 size = 2
Stack as string: [1, 2, ?, ?, ?, ?, ?, ?, ?, ?]
*/
